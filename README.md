# Dice Battle Projet

In this project we are interested in a stochastic game which consists of the following: 


Two players face each other, and throw a number between 1 and D of dice, if one of their dice rolls 1 they win 1 point otherwise they win the sum of the dice scores, the first one to have N wins the game.
The idea is that the more dice you throw, the more points you are likely to score, but the more likely you are to have at least one die that rolls 1


During the project we deal with two variants, a rather easy sequential one, where the players play one after the other and the first one is obviously at an advantage, and a second one where the players play simultaneously.


For the sequential variant we first proposed a sequential variant that maximises the expectation of the score at each roll independently of the opponent's score, for example, for D=10 the number of dice maximising the expectation of the score is 6.


However, if we consider the case where one of the players has 98 points and only needs 100 to win, we notice that six dice is not the best choice, because the player has every interest in minimising the probability of having one because it is the only case in which he does not win.


For the simultaneous variant, we started by solving a simplified version where we simply try to calculate an optimal mixed strategy (probability law on the number of dice that can be thrown) on a single roll and then by combining dynamic programming and linear programming we can determine a mixed strategy for each game state.

You can, of course, find all the details in the report(French).
